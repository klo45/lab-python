import unittest
from module import *
truck = Truck()


class truckTest(unittest.TestCase):

	def setUp(self):
	 	truck.fuel = 0

	def test_loadFull(self):
	 	truck.loadFuel("full")
	 	self.assertEqual(truck.fuel, 300 )

	def test_loadSome(self):
	 	truck.loadFuel(30)
	 	self.assertEqual(truck.fuel, 30)


	def test_combustion(self):
		self.assertEqual(truck.burningfuel, 10)

	def test_driving(self):
		truck.fuel = 10
		truck.driveForward(1)
		self.assertEqual(truck.fuel,0 )

	def test_drivingBack(self):
		truck.fuel = 10
		truck.driveBackward(1)
		self.assertEqual(truck.fuel,0 )

	def test_loadCargo(self):
		truck.loadCargo("tanks")
		self.assertEqual(truck.cargo,"tanks")

	def test_unloadCargo(self):
		truck.unloadCargo()
		self.assertEqual(truck.cargo,None)

if __name__ == '__main__':
    unittest.main()