import unittest
from module import *
car = Car()


class CarTest(unittest.TestCase):

	def setUp(self):
	 	car.fuel = 0

	def test_loadFull(self):
	 	car.loadFuel("full")
	 	self.assertEqual(car.fuel, 150)

	def test_loadSome(self):
	 	car.loadFuel(30)
	 	self.assertEqual(car.fuel, 30)


	def test_combustion(self):
		self.assertEqual(car.burningfuel, 1)

	def test_driving(self):
		car.fuel = 10
		car.driveForward(10)
		self.assertEqual(car.fuel,0 )

	def test_drivingBack(self):
		car.fuel = 10
		car.driveBackward(10)
		self.assertEqual(car.fuel,0 )



if __name__ == '__main__':
    unittest.main()